## 开发流程

1. 从最新的master切出任务分支，操作方法可以参见下面示例或用其它等价方案，以下branch_name指代需新建的分支名称
    1. git checkout master && git pull origin master && git checkout -b **branch_name**
    1. git fetch origin master:**branch_name** && git checkout **branch_name**
1. 在新分支进行开发
1. 将修改的代码提交并推送到远端
    1. git add **需提交的文件**  
        1. 不建议使用**.**来加入所有文件，除非非常确认本次所提交的内容
        1. 提交前可以使用git diff来查看修改的文件内容，确保所提交内容是所想提交的，特别是在解决冲突之后
    1. git commit -m **本次提交内容的说明**
        1. commit之后可以使用git log二次确认所提交内容
        1. 建议说明内容能够简述本次开发任务以及提交内容，以便后续查看
    1. git push --set-upstream origin **branch_name**
        1. push到远端后可以使用gitlab网页端查看提交内容，进行再次确认
    1. 多人合作在一个分支上进行开发时，需要时常git pull以同步他人的工作内容
        1. 在commit之前，直接使用pull；若出现冲突的场景，需先stash，然后pull，之后再stash pop，此时需要解决冲突
        1. 在commit之后，需要使用pull --rebase以使得提交树保持直线且没有merge_request（建议操作，log可以更清晰好看），此时可能需要解决冲突
        1. commit之前的冲突解决，只需要修正冲突文件，再继续add、commit即可
        1. commit之后的冲突解决，需要修正冲突文件，add之后，执行rebase --continue操作，如果冲突解决结果是完全放弃自己的提交需执行rebase --skip（情况较少见，此时可以参见git status的提示）
1. 功能上线前提交merge_request，由相关人员进行code review
    1. 当前建议在此时进行rebase master操作，命令：git pull --rebase origin master，之后强推远端分支：git push -f origin **branch_name**
        1. 优点：
            1. 可能和已上线功能出现冲突，此时可以一并解决并进行再次测试
            1. log会更加清晰好看
        1. 缺点：
            1. rebase会重写log，因此之后必须进行强推，这是个危险操作
            1. 如果当前分支与master有冲突，且冲突部分在整个功能开发过程中进行了非常多次提交，rebase过程中需多次解决冲突（有解决方案）
        1. 针对缺点的解决方案（降低风险，无法根治）
            1. 多人开发时，约定某个时间点由特定人进行rebase，之后强推，其余人都在本地进行pull --rebase操作以同步远端
            1. 多次需要解决冲突的场景，可以先将commit进行合并：git rebase -i **特定commit hash**
        
1. 功能被合并至master，由相关人员上线


## 建议
1. 分支名命名可以参照以下规范
    1. 使用英文单词命名，命名可以描述本次开发任务，专有中文除外
    1. 使用**-**连接各个英文单词
    1. 使用前缀标识分支性质
        1. feature代表新功能开发
        1. hotfix代表线上紧急问题修复
        1. bugfix代表一般bug修复
1. 不同功能的提交不要放在同一分支，基于任务去创建分支
1. 开发前后可以多次使用git diff或gitlab网页端的merge_request功能，来确认提交的代码内容无误（相当于自己进行code review，能解决明显的bug和一些设计缺陷等）


## 常见场景

1. 回撤某次本地提交
    1. 如果为最新一次提交：git reset HEAD^
    1. 如果为历史提交：git revert **commit hash**
1. 回撤某次远端提交：git revert **commit hash**，然后push
1. 回撤某次merge_request，git revert **merge commit hash** -m 1
1. 回撤已上线的某段提交
    1. 从最新的远端master切出一个新分支
    1. git checkout **commit1 hash**  (这个commit点是正常代码，之后是错误代码)
    1. git reset **commit2 hash**  (这个commit点是错误代码结束)
    1. git checkout到分支，此时工作区的修改内容是commit1 到 commit2直接的diff，然后可以提交并推送到远端
    ```
    注意事项：
    1. 一般适合于最新上线的一段代码有问题，直接回退到上一个版本（因为可以确认上一个版本的代码是可以正常使用的）
    1. 如果是操作任意两个commit之间的代码，存在代码本身不完整、不可用的情况（两个commit都是merge_request可以减少这个风险，因为一个merge_request标志着一个版本）
    ```
1. 删除本地无用(已合并至master)分支：git branch --merged=master|grep -v master|xargs -L 1 git branch -d
1. 删除远端无用分支(已合并至master)
    1. 在gitlab网页端可以一键删除所有已合并分支，之后在本地执行git fetch -p
    1. git branch -a --merged=master|xargs -L 1 git push -d origin